#include <iostream>
#include <string>
using namespace std;
// I've added a comment
int main()
{
    int hunger = 0;
    int health = 100;
    int happiness = 100;
    string petName = "";
    bool isDone = false;
    int menuChoice;


    cout << "Please enter your pet's name: ";
    cin >> petName;


    while( !isDone )
    {
        cout << "---------------------------------------------------------" << endl;
        cout << petName << endl;
        cout << "      " << "Hunger: " << hunger << "%";
        cout << "      " << "Health: " << health << "%";
        cout << "      " << "Happiness: " << happiness << "%" << endl;
        cout << "---------------------------------------------------------" << endl;

        cout << "OPTIONS: 1. Feed  2. Play  3. Vet  4. Quit" << endl;
        cout << "> ";
        cin >> menuChoice;

        switch ( menuChoice )
        {
            case 1:
                cout << "FOODS: 1. Pizza  2. Broccoli  3. Tuna" << endl;
                cin >> menuChoice;

                if ( menuChoice =  1)
                {
                    cout << "You fed " << petName << " pizza" << endl;
                    health -= 1;
                    hunger -= 15;
                }
                else if ( menuChoice = 2)
                {
                    cout << "You fed " << petName << " broccoli" << endl;
                    health += 1;
                    hunger -= 15;
                }
                else if ( menuChoice = 3)
                {
                    cout << "You fed " << petName << " tuna" << endl;
                    health += 2;
                    hunger -= 12;
                }
                break;


            case 2:
                cout << "GAMES: 1. Fetch  2. Tug-of-war  3. Videogames" << endl;
                cin >> menuChoice;

                if ( menuChoice =  1)
                {
                    cout << "You played fetch with " << petName << "!" << endl;
                    happiness += 8;
                    health += 2;

                }
                else if ( menuChoice = 2)
                {
                    cout << "You played tug-of-war with " << petName << "!" << endl;
                    happiness += 9;
                }
                else if ( menuChoice = 3)
                {
                    cout << "You played videogames with " << petName << "!" << endl;
                    happiness += 10;
                    health -= 1;
                }
                break;

            case 3:

                cout << "The vet says: " << endl;

                if ( happiness < 50)
                {
                    cout << "Make sure to play with " << petName << " more." << endl;
                }
                else if ( hunger > 50)
                {
                    cout << "Make sure to feed " << petName << "." << endl;
                }
                else if ( health < 50)
                {
                    cout << petName << " isn't looking healthy. Take better care of it!" << endl;
                }
                else if ( happiness >= 50 && hunger <= 50 && health >= 50)
                {
                    cout << petName << " is looking OK!" << endl;
                }
                break;

            case 4:

                cout << "Are you sure you want to quit?" << endl;
                cout << "1. Quit        2. Don't quit" << endl;
                cin >> menuChoice;

                if ( menuChoice = 1 )
                {
                    isDone = true;
                }
                break;
        }

        hunger += 5;

        if ( hunger > 50)
        {
           happiness -= 10;
           health    -= 10;
        }
        else
        {
            happiness -=5;
        }

        if      ( happiness < 0)   { happiness = 0;}
        else if ( happiness > 100) { happiness = 100;}

        if      ( hunger < 0)   { hunger = 0;}
        else if ( hunger > 100) { hunger = 100;}

        if      ( health > 100)   { health = 100;}
        else if ( health < 0)
        {
            health = 0;
            if (health <= 0)
            {
                cout << "You haven't taken care of " << petName << "!" << endl;
                cout << petName << " has been removed from your care." << endl;
                isDone = true;
            }
        }
    }


    return 0;
}
