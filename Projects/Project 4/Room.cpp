#include "Room.hpp"
#include <iostream>
using namespace std;

Room::Room()
{
    m_ptrNeighborWest = nullptr;
    m_ptrNeighborEast = nullptr;
    m_ptrNeighborNorth = nullptr;
    m_ptrNeighborSouth = nullptr;
}

void Room::Display()
{
    cout << "--------------------------------" << endl;
    cout << m_name << endl;
    cout << m_description << endl;

    cout << "You can go: ";

    if ( m_ptrNeighborEast != nullptr )
    {
        cout << "East ";
    }

    if ( m_ptrNeighborWest != nullptr )
    {
        cout << "West ";
    }

    if ( m_ptrNeighborNorth != nullptr )
    {
        cout << "North ";
    }

    if ( m_ptrNeighborSouth != nullptr )
    {
        cout << "South ";
    }

    cout << endl << endl;
}
